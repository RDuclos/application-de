package com.example.des;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {
    private Button btLancer;
    private Spinner SpFaces;
    private int ChoixFaces;
    private Spinner SpNbDes;
    private int ChoixNbDes;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btLancer = (Button) findViewById(R.id.btlancer);
        SpFaces = (Spinner) findViewById(R.id.SpNombre__face);
        SpNbDes = (Spinner) findViewById(R.id.SpNombre_des);

        //evenements listener button
        btLancer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toRecap();
            }
        });

        //installation des spinner
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.faces,android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,R.array.nb_des,android.R.layout.simple_spinner_item);

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        SpFaces.setAdapter(adapter1);
        SpNbDes.setAdapter(adapter2);
        SpFaces.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                ChoixFaces = Integer.parseInt(adapterView.getItemAtPosition(position).toString()) ;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        SpNbDes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                ChoixNbDes = Integer.parseInt(adapterView.getItemAtPosition(position).toString()) ;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        }

        );
    }
    public void toRecap(){
        Intent intent = new Intent(this,Activity_Affichage_des_resultats.class);
        intent.putExtra("Faces", ChoixFaces);
        intent.putExtra("Nombre", ChoixNbDes);
        startActivity(intent);
    }
}