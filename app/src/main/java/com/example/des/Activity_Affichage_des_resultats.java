package com.example.des;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.security.SecureRandom;

public class Activity_Affichage_des_resultats extends AppCompatActivity {
    public TextView TvResultats;
    public String message;
    public int Faces;
    public int NbDes;
    public Button boutonrecommencer;
    public Button boutonretour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__affichage_des_resultats);
        TvResultats = (TextView) findViewById(R.id.tv_resultatlok);
        boutonrecommencer = (Button) findViewById(R.id.buttonrelancer);
        boutonretour = (Button) findViewById(R.id.boutonretour);


        Intent mIntent = getIntent();
        Faces = mIntent.getIntExtra("Faces", 6);
        NbDes = mIntent.getIntExtra("Nombre", 1);
        lancer_des();
        boutonrecommencer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lancer_des();
            }
        });
        boutonretour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }
    public void lancer_des()
    {

        SecureRandom Generator = new SecureRandom();
        message = "Résultats : ";
        int cpt = 0;

        while(cpt < NbDes)
        {
            message += " " + (Generator.nextInt(Faces)+1);
            cpt = cpt+1;
        }
        TvResultats.setText(message);

    }

}